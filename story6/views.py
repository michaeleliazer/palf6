from django.shortcuts import render

from .forms import FormKegiatan
from .models import Kegiatan

# Create your views here.
def index(request):
    form_kegiatan = FormKegiatan()
    context = {
        'form_kegiatan' : form_kegiatan,
    }

    return render(request, 'kegiatan.html', context)

def create(request):
	form_kegiatan = PostForm()

	if request.method == 'POST':
		PostModel.objects.create(
				judul 		= request.POST.get('judul'),
				body		= request.POST.get('body'),
				category	= request.POST.get('category'),
			)

		return HttpResponseRedirect("/blog/")


	context = {
		'page_title':'create post',
		'post_form':post_form
	}

	return render(request,'blog/create.html',context)