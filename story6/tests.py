from django.test import TestCase
from django.urls import resolve
from .models import Kegiatan
from .views import index
from .forms import FormKegiatan
# Create your tests here.

class Story6tUnitTest(TestCase):

    def test_story6_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_create_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = 'perak2020')
        jumlah = Kegiatan.objects.all().count()
        self.assertEqual(jumlah , 1)

    def test_story6_using_right_template(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'kegiatan.html')

    def test_nama_fungsi_views(self):
        found = resolve('/home/')
        self.assertEqual(found.func , index)

    #def test_nama_kegiatan_notNull(self):
     #   response = self.client.post("/tambah/kegiatan/", data={"nama":""})
      #  self.assertEqual(response.status_code, 200)
       # self.assertContains(response,)

    def test_forms(self):
        form_data = {'nama_kegiatan': 'perak2020'}
        form = FormKegiatan(data=form_data)
        self.assertTrue(form.is_valid())