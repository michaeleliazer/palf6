from django import forms

class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(
      label='Nama Kegiatan',
      max_length=30,
      widget= forms.TextInput(
         attrs={
          'class':'form-control',
          'placeholder':'isi nama kegiatan anda',}
      )
    )
    deskripsi = forms.CharField(
      widget= forms.Textarea(
        attrs={
          'class':'form-control',
          'placeholder':'isi dengan penjelasan kegiatan anda',
        })
    )
    TAHUN = range(2000,2031,1)
    tanggal_kegiatan = forms.DateField(
        widget=forms.SelectDateWidget(
        attrs={
          'class':'form-control col-sm-2',
        },
        years=TAHUN)
    )
    partisipan = forms.CharField(
      widget= forms.Textarea(
        attrs={
          'class':'form-control',
          'placeholder':'isi dengan penjelasan kegiatan anda',
        })
    )
    password = forms.CharField(
      widget=forms.PasswordInput(
        attrs={
          'class':'form-control',
          'placeholder':'Password',
        }
      )
      )