from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan		= models.CharField(max_length = 30)
    deskripsi       	= models.TextField()
    tanggal_kegiatan	= models.DateField()
    partisipan       	= models.CharField()
    password           	= models.CharField()

    published	= models.DateTimeField(auto_now_add = True)
    updated		= models.DateTimeField(auto_now = True)

    def __str__(self):
        return "{}. {}".format(self.id, self.nama_kegiatan)